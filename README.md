# ratbv-service-alerts

Specifications for the Service Alerts API:
* API should return JSON or XML
* For each alert the following data should be supplied:
  1. Short title of the alert - max ~40 characters. 
  2. Body of the alert - unlimited length and could possibly include HTML elements.
  3. List of routes that the alert applies to - should match the route names in the editor.
  4. Publish date (optional)
  5. Date to remove (optional)
  6. Status - The operating status of the routes, for example: 0 = "Normal Operation", 1 = "Irregular Operation", 2 = "Not Operating" (optional)
  7. Alert ID (optional)
